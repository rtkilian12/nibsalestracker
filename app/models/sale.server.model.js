'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Sale Schema
 */
var SaleSchema = new Schema({
	// Sale model fields   
	// ...
});

mongoose.model('Sale', SaleSchema);